package objetos.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetos.Capacitor;
import objetos.Componente;
import objetos.Inductor;
import objetos.Resistencia;

public class ComponenteTest {
	//defino el lote de pruebas
	Inductor inductorVacio;
    Inductor inductorConDatos;
    
    Capacitor capacitorVacio;
    Capacitor capacitorConDatos;
    
    Resistencia resistenciaVacia;
    Resistencia resistenciaConDatos;
    
    List<Componente> componentesList;
    Set<Componente>  componentesSet;
    
    
	@Before
	public void setUp() throws Exception {
		//se ejecuta antes de cada testeo
		//creo los objetos
		inductorVacio 		= new Inductor()				 ;
		inductorConDatos 	= new Inductor("L1",0.003f, 3000);
	
		capacitorVacio 		= new Capacitor();
		capacitorConDatos   = new Capacitor("C1", 0.00001f,1000);
		
		resistenciaVacia	= new Resistencia();
		resistenciaConDatos = new Resistencia("R1", 100);
		
		componentesList = new ArrayList<>();		
		componentesList.add(new Inductor("indu1", 0.1f, 100));
		componentesList.add(new Inductor("indu2", 0.2f, 200));
		componentesList.add(new Inductor("indu3", 0.3f, 300));
		componentesList.add(new Inductor("indu4", 0.4f, 400));
		componentesList.add(new Inductor("indu5", 0.5f, 500));
		componentesList.add(new Inductor("indu6", 0.6f, 600));
		componentesList.add(new Inductor("indu7", 0.7f, 700));
		componentesList.add(new Inductor("indu8", 0.8f, 800));
		componentesList.add(new Inductor("indu9", 0.9f, 900));
		componentesList.add(new Inductor())					;
		
		componentesList.add(new Capacitor("cap1",0.001f,1000));
		componentesList.add(new Capacitor("cap2",0.002f,2000));
		componentesList.add(new Capacitor("cap3",0.003f,3000));
		componentesList.add(new Capacitor("cap4",0.004f,4000));
		componentesList.add(new Capacitor("cap5",0.005f,5000));
		componentesList.add(new Capacitor("cap6",0.006f,6000));
		componentesList.add(new Capacitor("cap7",0.007f,7000));
		componentesList.add(new Capacitor());
		
		componentesList.add(new Resistencia("Res1", 100));
		componentesList.add(new Resistencia("Res2", 200));
		componentesList.add(new Resistencia("Res3", 300));
		componentesList.add(new Resistencia("Res4", 400));
		componentesList.add(new Resistencia("Res5", 500));
		componentesList.add(new Resistencia("Res6", 600));
		componentesList.add(new Resistencia());
		
		componentesSet = new HashSet<>();
		componentesSet.add(new Inductor("indu1", 0.1f, 100));
		componentesSet.add(new Inductor("indu2", 0.2f, 200));
		componentesSet.add(new Inductor("indu3", 0.3f, 300));
		componentesSet.add(new Inductor("indu4", 0.4f, 400));
		componentesSet.add(new Inductor("indu5", 0.5f, 500));
		componentesSet.add(new Inductor("indu6", 0.6f, 600));
		componentesSet.add(new Inductor("indu7", 0.7f, 700));
		componentesSet.add(new Inductor("indu8", 0.8f, 800));
		componentesSet.add(new Inductor("indu9", 0.9f, 900));
		componentesSet.add(new Inductor())					;
		
		componentesSet.add(new Capacitor("cap1",0.001f,1000));
		componentesSet.add(new Capacitor("cap2",0.002f,2000));
		componentesSet.add(new Capacitor("cap3",0.003f,3000));
		componentesSet.add(new Capacitor("cap4",0.004f,4000));
		componentesSet.add(new Capacitor("cap5",0.005f,5000));
		componentesSet.add(new Capacitor("cap6",0.006f,6000));
		componentesSet.add(new Capacitor("cap7",0.007f,7000));
		componentesSet.add(new Capacitor());
		
		componentesSet.add(new Resistencia("Res1", 100));
		componentesSet.add(new Resistencia("Res2", 200));
		componentesSet.add(new Resistencia("Res3", 300));
		componentesSet.add(new Resistencia("Res4", 400));
		componentesSet.add(new Resistencia("Res5", 500));
		componentesSet.add(new Resistencia("Res6", 600));
		componentesSet.add(new Resistencia());
		
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		//despues de cada testeo
		inductorVacio		=null;
		inductorConDatos	=null;
		
		capacitorVacio  	=null;
		capacitorConDatos 	=null;
		
		resistenciaVacia 	=null;
		resistenciaConDatos =null;
		
		componentesList		=null;
		componentesSet 		=null;
	}
   @Test
   public void testAdd_Set_TRUE_noLoDeveriaAgregar(){
		assertFalse(componentesSet.add(new Capacitor()));
	}

   @Test
   public void testAdd_Set_FALSE_agregandooo(){
	    Capacitor cap = new Capacitor();
	    cap.setNombre("Gabrielto");
		assertTrue(componentesSet.add(cap));
	}

	@Test
	public void testEquals_Capacitor_Contains_TRUE(){
		Capacitor otro = new Capacitor();
		assertTrue(componentesList.contains(otro));
	}
	
	@Test
	public void testEquals_Capacitor_Contains_FALSE(){
		Capacitor otro = new Capacitor();
		otro.setFrecuencia(7000);
		assertFalse(componentesList.contains(otro));
	}
	
	//voy a probar solamente el nombre ustedes deberian porbar el resto de los atribuos
	@Test
	public void testCapacitorNombre(){
		assertEquals("cap_def", capacitorVacio.getNombre());
	}

	@Test
	public void testInductorNombre() {
		assertEquals("L por defecto", inductorVacio.getNombre());
	}
	//TODO alumnos, prueben el inductConDatosNombre
	@Test
	public void testInductoUnidad() {
		assertEquals("Hy", inductorVacio.getUnidad());
	}
	//TODO alumnos, prueben el inductConDatosUnidad
	@Test
	public void testInductorInductancia(){
		assertEquals(0.01f, inductorVacio.getValor(), 0.001);
	}
	//TODO alumnos, prueben el inductConDatosInductancia
	@Test
	public void testInductorFrecuencia() {
		assertEquals(1000, inductorVacio.getFrecuencia());
		
	}
	//TODO alumnos, prueben el inductConDatosFrecuencia
	@Test
	public void testCalcularImpedanciaInductor() {
		assertEquals(62.83f, inductorVacio.calcularImpedancia(), 0.01);
		
	}
	//TODO alumnos, prueben el inductConDatosImperanccia
	@Test
	public void testEqalsTRUE(){
		Inductor otro = new Inductor();
		assertTrue(inductorVacio.equals(otro));
	}
	//TODO alumnos, prueben el inductConDatosEquasTrue
	@Test
	public void testEqalsFALSE(){
		Inductor otro = new Inductor();
		otro.setNombre("otro");
		assertFalse(inductorVacio.equals(otro));
	}
	//TODO alumnos, prueben el inductConDatosEquasFalse
	@Test
	public void testEqualsEnListaTRUE(){
		assertTrue(componentesList.contains(inductorVacio));
	}
	//TODO alumnos, prueben el inductConDatosListaEquasTrue
	@Test
	public void testEqualsEnListaFALSE(){
		assertFalse(componentesList.contains(inductorConDatos));
	}
	//TODO alumnos, prueben el inductConDatosEquasFalse
}
